// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyCYva6HoF0tDNvm6wuGF_EqH_gcD02Lto4",
    authDomain: "example2-d9bf6.firebaseapp.com",
    projectId: "example2-d9bf6",
    storageBucket: "example2-d9bf6.appspot.com",
    messagingSenderId: "752266217286",
    appId: "1:752266217286:web:aba9fffd4cb13063ad4747"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
