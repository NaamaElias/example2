import { Post } from './../interfaces/post';
import { PostsService } from './../posts.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Observable } from 'rxjs';


@Component({
  selector: 'posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  panelOpenState = false;
  posts$:Observable<Post>;
  userId:string;
  saved = [];
  

  constructor(private postsService:PostsService, public authService:AuthService) { }

  showComments(id){
     this.postsService.postCommentsData(id);
  }

  add(i, title, body){
    this.postsService.addPost(this.userId,title,body);
    this.saved[i] = !this.saved[i];
  }

  ngOnInit(): void {
    this.posts$ = this.postsService.postsData();
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
      }
    )

  }

}
