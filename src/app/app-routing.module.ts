import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommentsFormComponent } from './comments-form/comments-form.component';
import { LoginComponent } from './login/login.component';
import { PostsComponent } from './posts/posts.component';
import { SavedComponent } from './saved/saved.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { WelcomeComponent } from './welcome/welcome.component';

const routes: Routes = [
  {path: '', component: WelcomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'signup', component: SignUpComponent},
  {path: 'posts', component: PostsComponent},
  {path: 'comments', component: CommentsFormComponent},
  {path: 'saved', component: SavedComponent},
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
