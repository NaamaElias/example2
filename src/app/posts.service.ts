import { Post } from './interfaces/post';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})
export class PostsService {

  postCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users');

  constructor(private http:HttpClient, private db:AngularFirestore) { }

  postsData():Observable<Post>{
    return this.http.get<Post>('https://jsonplaceholder.typicode.com/posts');
  }

  CommentsData():Observable<any>{
    return this.http.get<any>(`https://jsonplaceholder.typicode.com/comments`);
  }

  postCommentsData(id:string){
    return this.http.get(`https://jsonplaceholder.typicode.com/posts/${id}/comments`);
  }

  addPost(userId:string,title:string,body:string){
     const post = {title:title, body:body};
     this.userCollection.doc(userId).collection('posts').add(post);
  }

  getposts(userId):Observable<any[]>{
    this.postCollection = this.db.collection(`users/${userId}/posts`);
    return this.postCollection.snapshotChanges();
  } 
  
   deleteSaved(userId:string,postId:string){
    this.db.doc(`users/${userId}/posts/${postId}`).delete();
  }

  updatelike(userId:string,postId:string,likes){
    this.db.doc(`users/${userId}/posts/${postId}`).update({
      likes:likes
    });
  }

}
