import { Post } from './../interfaces/post';
import { AuthService } from './../auth.service';
import { PostsService } from './../posts.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'saved',
  templateUrl: './saved.component.html',
  styleUrls: ['./saved.component.css']
})
export class SavedComponent implements OnInit {

  userId:string;
  saved$;
  saved:Post[];
  

  constructor(private postsService:PostsService, public authService:AuthService) { }

  delete(postId){
    this.postsService.deleteSaved(this.userId, postId);
  }

  update(id, likes:number){
    likes = likes+1;
    this.postsService.updatelike(this.userId, id, likes);
    }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.saved$ = this.postsService.getposts(this.userId);
        this.saved$.subscribe(
          docs => {
            this.saved = [];
            for (let document of docs){
              const saved = document.payload.doc.data();
              if(!saved.likes){
                saved.likes = 0;
              }
              saved.id = document.payload.doc.id;
              this.saved.push(saved);
            }
          }
        )
      }
    )
  }

}
