import { PostsService } from './../posts.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'commentsform',
  templateUrl: './comments-form.component.html',
  styleUrls: ['./comments-form.component.css']
})
export class CommentsFormComponent implements OnInit {

  postId:string;
  comments$:Observable<any>;

  constructor(private postsService:PostsService) { }

  ngOnInit(): void {
    this.comments$ = this.postsService.CommentsData();
  }

}
